#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <cuda.h>
#include <helper_functions.h>  // helper for shared functions common to CUDA Samples
#include <helper_cuda.h>       // helper functions for CUDA error checking and initialization

#define MEMCOPY_ITERATIONS  100
#define DEFAULT_SIZE        ( 32 * ( 1 << 20 ) )    //32 M
#define DEFAULT_INCREMENT   (1 << 22)               //4 M
#define CACHE_CLEAR_SIZE    (1 << 24)               //16 M

//shmoo mode defines
#define SHMOO_MEMSIZE_MAX     (1 << 26)         //64 M
#define SHMOO_MEMSIZE_START   (1 << 10)         //1 KB
#define SHMOO_INCREMENT_1KB   (1 << 10)         //1 KB
#define SHMOO_INCREMENT_2KB   (1 << 11)         //2 KB
#define SHMOO_INCREMENT_10KB  (10 * (1 << 10))  //10KB
#define SHMOO_INCREMENT_100KB (100 * (1 << 10)) //100 KB
#define SHMOO_INCREMENT_1MB   (1 << 20)         //1 MB
#define SHMOO_INCREMENT_2MB   (1 << 21)         //2 MB
#define SHMOO_INCREMENT_4MB   (1 << 22)         //4 MB
#define SHMOO_LIMIT_20KB      (20 * (1 << 10))  //20 KB
#define SHMOO_LIMIT_50KB      (50 * (1 << 10))  //50 KB
#define SHMOO_LIMIT_100KB     (100 * (1 << 10)) //100 KB
#define SHMOO_LIMIT_1MB       (1 << 20)         //1 MB
#define SHMOO_LIMIT_16MB      (1 << 24)         //16 MB
#define SHMOO_LIMIT_32MB      (1 << 25)         //32 MB

// Ehsan's defines 
#define NUM_OF_MODES 4
#define NUM_OF_ITERATIONS 4 

//enums, project // Imight use these // I got them from bandwidthtest.cu of samples.
enum testMode   { QUICK_MODE, RANGE_MODE, SHMOO_MODE };
enum memcpyKind { DEVICE_TO_HOST, HOST_TO_DEVICE, DEVICE_TO_DEVICE };
enum printMode  { USER_READABLE, CSV };
enum memoryMode { PINNED, PAGEABLE };

using namespace std;
//Source: http://acceleware.com/blog/nvidia-cuda-60-unified-memory-performance
__global__ void  loop (int * a,int*b)//,float * time_out
{

//Note that you can not call CudaMalocMannaged hewr because it is a __host__ function. 

	for ( int i = 0; i < MEMCOPY_ITERATIONS; i++)
	{
		*a=*b;
	}
}


int main (int argc, char **argv)
{
int mode = 3; // This would determine the switch case statement. 

// Checking the number of arguments
if (argc > 2)
{
	printf("Too many arguments.\n The second argument is a mode (from 0 to %d).\n",NUM_OF_MODES);
}
if (argc < 1)
{
	printf("Too few arguments.\n Please selecte a mode (from 0 to %d).\n",NUM_OF_MODES);
}



// Read the mode before you change the pointer
mode = atoi(*(argv+1));

//Reading the input arguments
printf("List of input arguments:\n"); 
int i =0;
while(i<argc)
{
	printf("\t%d  %s\n",i, *argv++);
	i++;
} 


if (mode > (NUM_OF_MODES-1))
{
	printf("Wrong mode number is inserted. Modes should be below %d.\n",NUM_OF_MODES);
}
else 
{
	printf("Selected mode is %d.\n\n",mode);	
}


int increment = DEFAULT_INCREMENT; // I did not use this variable 

float bandwidthInMBs = 0.0f;
unsigned int memSize = 33554432;


int *p_a, *p_b;


//cudaMallocManaged(&p_a,sizeof(int)*memSize);
//cudaMallocManaged(&p_b,sizeof(int)*memSize);



switch (mode)
{
/////////////////////////////////////////////////////////////////
// Copying from pointer to pointer in unified memory. 
// This is done in the host.
// No handled memcopy functions was used. 
/////////////////////////////////////////////////////////////////
case 0: 
 	cudaMallocManaged(&p_a,sizeof(int)*memSize);
	cudaMallocManaged(&p_b,sizeof(int)*memSize);

          
   	printf("Running mode %d. memsize = %d\n",mode,memSize);
    
	// memSize=1
	cudaEvent_t start, stop;
	float time;

	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	cudaEventRecord( start, 0 );

	cudaDeviceSynchronize();

	//Do some access memory on Host
	for ( int i = 0 ; i < MEMCOPY_ITERATIONS; i++)
	{
		*p_a=*p_b;
	}


	//get the the total elapsed time in ms
	cudaEventRecord( stop, 0 );
	cudaEventSynchronize( stop );

	cudaEventElapsedTime( &time, start, stop );
	cudaEventDestroy( start );
	cudaEventDestroy( stop );


	//calculate bandwidth in MB/s
	bandwidthInMBs = ( sizeof(int)* (float)MEMCOPY_ITERATIONS) / ((float)time );
	printf("Elapsed time for copyig in host is: %f ms.\n",time);
	printf("Bandwidth in MB/s is: %f\n",bandwidthInMBs/1000);

	cudaFree(p_a);
	cudaFree(p_b);

	break; 

/////////////////////////////////////////////////////////////////
// Copying from simple counter to a pointer in unified memory. 
// No handled memcopy functions was used. 
/////////////////////////////////////////////////////////////////
case 1:
 	cudaMallocManaged(&p_a,sizeof(int)*memSize);
	cudaMallocManaged(&p_b,sizeof(int)*memSize);

   	printf("Running mode %d. memsize = %d\n",mode,memSize);
     
	// memSize=1
	cudaEvent_t start_1, stop_1;
	float time_1;

	cudaEventCreate(&start_1);
	cudaEventCreate(&stop_1);

	cudaEventRecord( start_1, 0 );

	cudaDeviceSynchronize();

	//Do some access memory on Host
	for ( int i = 0 ; i < MEMCOPY_ITERATIONS; i++)
	{
		*p_a=i;
	}


	//get the the total elapsed time in ms
	cudaEventRecord( stop_1, 0 );
	cudaEventSynchronize( stop_1 );

	cudaEventElapsedTime( &time_1, start_1, stop_1 );
	cudaEventDestroy( start_1 );
	cudaEventDestroy( stop_1 );


	//calculate bandwidth in MB/s
	bandwidthInMBs = ( memSize *sizeof(int)* (float)MEMCOPY_ITERATIONS) / ((float)time_1 );
	printf("Elapsed time for copyig i to uniffied memory is: %f\n",time_1);
	printf("Bandwidth in MB/s is: %f\n",bandwidthInMBs/1000);

	cudaFree(p_a);
	cudaFree(p_b);

	break; 


/////////////////////////////////////////////////////////////////
// Copying from pointer to pointer in unified memory.
// This is done in device using <<<1,1>>> configuration.  
// No handled memcopy functions was used. 
/////////////////////////////////////////////////////////////////
case 2:
	cudaMallocManaged(&p_a,sizeof(int)*memSize);
	cudaMallocManaged(&p_b,sizeof(int)*memSize);

	printf("Running mode %d. memsize = %d\n",mode,memSize);
    
	// memSize=1
	cudaEvent_t start_2, stop_2;
	float time_2;
	cudaEventCreate(&start_2);

	cudaEventCreate(&stop_2);

	cudaEventRecord( start_2, 0 );

	loop<<<1,1>>>(p_a,p_b);

	cudaDeviceSynchronize();

	//get the the total elapsed time in ms
	cudaEventRecord( stop_2, 0 );
	cudaEventSynchronize( stop_2 );

	cudaEventElapsedTime( &time_2, start_2, stop_2 );
	cudaEventDestroy( start_2 );
	cudaEventDestroy( stop_2 );

	//calculate bandwidth in MB/s
	bandwidthInMBs = ( memSize *sizeof(int)* (float)MEMCOPY_ITERATIONS) / ((float)time_2 );
	printf("Elapsed time for copying in device is: %f ms\n",time_2); 
	printf("Bandwidth in MB/s is: %f\n",bandwidthInMBs/1000);	
	
	cudaFree(p_a);
	cudaFree(p_b);
								
	break; 

/////////////////////////////////////////////////////////////////
// Copying from pointer to pointer in unified memory. 
// Handled memcopy functions was used. memCpy() was used.
/////////////////////////////////////////////////////////////////
case 3: 
	cudaMallocManaged(&p_a,sizeof(int)*memSize);
	cudaMallocManaged(&p_b,sizeof(int)*memSize);


    	StopWatchInterface *timer = NULL;
    	float elapsedTimeInMs = 0.0f;
    	cudaEvent_t start_3, stop_3;

	sdkCreateTimer(&timer);
    	checkCudaErrors(cudaEventCreate(&start_3));
    	checkCudaErrors(cudaEventCreate(&stop_3));
    
    	printf("Running mode %d. memsize = %d\n",mode,memSize);
    
    	//copy data from GPU to Host
    	sdkStartTimer(&timer);
    	checkCudaErrors(cudaEventRecord(start_3, 0));
    
    	//Do some access memory on Host
	for ( int i = 0 ; i < MEMCOPY_ITERATIONS; i++)
	{
		memcpy (p_a,p_b,memSize * sizeof(int));
	}

	cudaDeviceSynchronize();

    	//get the the total elapsed time in ms
    	sdkStopTimer(&timer);
    	cudaEventElapsedTime(&elapsedTimeInMs, start_3, stop_3);

	elapsedTimeInMs = sdkGetTimerValue(&timer);

	//calculate bandwidth in MB/s
	bandwidthInMBs = ( memSize * (float)MEMCOPY_ITERATIONS) / ((float)elapsedTimeInMs );
	printf("Elapsed time for copying using Memcpy is: %f ms\n",elapsedTimeInMs); 
	printf("Bandwidth in MB/s is: %f\n",bandwidthInMBs/1000);


    	//clean up memory
    	checkCudaErrors(cudaEventDestroy(stop_3));
    	checkCudaErrors(cudaEventDestroy(start_3));
    	sdkDeleteTimer(&timer);
	
	cudaFree(p_a);
	cudaFree(p_b);

	break; 
}

/////////////////////////////////////////////////////////////////
// Copying from pointer to pointer in unified memory. 
// Handled memcopy functions was used. memCpy() was used.
// In case 4, I performed multiple interations of case 3 with different memsizes. 
/////////////////////////////////////////////////////////////////
case 4: 
	memSize = 8192;
printf ("Running mode %d. This mode copies data in variable chuncks.\n");
	for(int i=1; i =< NUM_OF_ITERATIONS ; i++ ) {
		
		//Trying different sizes for memSize.
		memSize=i*memSize;

		cudaMallocManaged(&p_a,sizeof(int)*memSize);
		cudaMallocManaged(&p_b,sizeof(int)*memSize);
		

    		StopWatchInterface *timer = NULL;
    		float elapsedTimeInMs = 0.0f;
    		cudaEvent_t start_3, stop_3;

		sdkCreateTimer(&timer);
    		checkCudaErrors(cudaEventCreate(&start_3));
    		checkCudaErrors(cudaEventCreate(&stop_3));
    
    		printf("Copying data in chucnks of \"memsize\" = %d\n",mode,memSize);
    
    		//copy data from GPU to Host
    		sdkStartTimer(&timer);
    		checkCudaErrors(cudaEventRecord(start_3, 0));
    
    		//Do some access memory on Host
		for ( int i = 0 ; i < MEMCOPY_ITERATIONS; i++)
		{
			memcpy (p_a,p_b,memSize * sizeof(int));
		}

		cudaDeviceSynchronize();

    		//get the the total elapsed time in ms
    		sdkStopTimer(&timer);
    		cudaEventElapsedTime(&elapsedTimeInMs, start_3, stop_3);

		elapsedTimeInMs = sdkGetTimerValue(&timer);

		//calculate bandwidth in MB/s
		bandwidthInMBs = ( memSize * (float)MEMCOPY_ITERATIONS) / ((float)elapsedTimeInMs );
		printf("Elapsed time for copying using Memcpy is: %f ms\n",elapsedTimeInMs); 
		printf("Bandwidth in MB/s is: %f\n",bandwidthInMBs/1000);


    		//clean up memory
    		checkCudaErrors(cudaEventDestroy(stop_3));
    		checkCudaErrors(cudaEventDestroy(start_3));
    		sdkDeleteTimer(&timer);
	
		cudaFree(p_a);
		cudaFree(p_b);

		break;
	} 
}

return 0;
}






