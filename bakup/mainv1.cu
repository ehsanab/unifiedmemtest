#include <stdlib.h>
#include <stdio.h>
#include <cuda.h>

#define MEMCOPY_ITERATIONS  10
using namespace std;
//Source: http://acceleware.com/blog/nvidia-cuda-60-unified-memory-performance
__global__ void AddIntSumCuda (int * a, int *b)
{
a[0] +=b[0];
}

__global__ void AplusB( int *ret, int a, int b) {
   ret[threadIdx.x] = a + b + threadIdx.x;
}

int main ()
{
float bandwidthInMBs = 0.0f;
 unsigned int memSize = 1;
int a = 5;
int b = 10;

int *p_a, *p_b;

cudaMallocManaged(&p_a,sizeof(int));
cudaMallocManaged(&p_b,sizeof(int));

p_a = &a;
p_b = &b;


cudaEvent_t start, stop;
float time;

cudaEventCreate(&start);
cudaEventCreate(&stop);

cudaEventRecord( start, 0 );


//sdkCreateTimer(&timer);
//checkCudaErrors(cudaEventCreate(&start));
//checkCudaErrors(cudaEventCreate(&stop));


cudaDeviceSynchronize();

for ( int i = 0; i < MEMCOPY_ITERATIONS; i++)
{
	*p_a=i;
}


//get the the total elapsed time in ms
cudaEventRecord( stop, 0 );
cudaEventSynchronize( stop );

cudaEventElapsedTime( &time, start, stop );
cudaEventDestroy( start );
cudaEventDestroy( stop );


//calculate bandwidth in MB/s
bandwidthInMBs = ( memSize * (float)MEMCOPY_ITERATIONS) /
             (time );
printf("Elapsed time is: %f\n",bandwidthInMBs);

cudaFree(p_a);
cudaFree(p_b);

return 0;
}






