#include <stdlib.h>
#include <stdio.h>
#include <cuda.h>
#include <cassert>

#define MEMCOPY_ITERATIONS  10
#define DEFAULT_SIZE        ( 32 * ( 1 << 20 ) )    //32 M
#define DEFAULT_INCREMENT   (1 << 22)               //4 M
#define CACHE_CLEAR_SIZE    (1 << 24)               //16 M

//shmoo mode defines
#define SHMOO_MEMSIZE_MAX     (1 << 26)         //64 M
#define SHMOO_MEMSIZE_START   (1 << 10)         //1 KB
#define SHMOO_INCREMENT_1KB   (1 << 10)         //1 KB
#define SHMOO_INCREMENT_2KB   (1 << 11)         //2 KB
#define SHMOO_INCREMENT_10KB  (10 * (1 << 10))  //10KB
#define SHMOO_INCREMENT_100KB (100 * (1 << 10)) //100 KB
#define SHMOO_INCREMENT_1MB   (1 << 20)         //1 MB
#define SHMOO_INCREMENT_2MB   (1 << 21)         //2 MB
#define SHMOO_INCREMENT_4MB   (1 << 22)         //4 MB
#define SHMOO_LIMIT_20KB      (20 * (1 << 10))  //20 KB
#define SHMOO_LIMIT_50KB      (50 * (1 << 10))  //50 KB
#define SHMOO_LIMIT_100KB     (100 * (1 << 10)) //100 KB
#define SHMOO_LIMIT_1MB       (1 << 20)         //1 MB
#define SHMOO_LIMIT_16MB      (1 << 24)         //16 MB
#define SHMOO_LIMIT_32MB      (1 << 25)         //32 MB

//enums, project // Imight use these // I got them from bandwidthtest.cu of samples.
enum testMode   { QUICK_MODE, RANGE_MODE, SHMOO_MODE };
enum memcpyKind { DEVICE_TO_HOST, HOST_TO_DEVICE, DEVICE_TO_DEVICE };
enum printMode  { USER_READABLE, CSV };
enum memoryMode { PINNED, PAGEABLE };

using namespace std;
//Source: http://acceleware.com/blog/nvidia-cuda-60-unified-memory-performance
__global__ void loop (int * a)
{
	for ( int i = 0; i < MEMCOPY_ITERATIONS; i++)
	{
		*a=i;
	}
}


int main ()
{
//int start = DEFAULT_SIZE;
//int end = DEFAULT_SIZE;
int increment = DEFAULT_INCREMENT;

float bandwidthInMBs = 0.0f;
unsigned int memSize = 33554432;

bool wc = false;
int *unified_mem;

unsigned char *h_idata = NULL;
unsigned char *h_odata = NULL;

memoryMode memMode =PINNED;

//allocate host memory
if (PINNED == memMode)
{
//pinned memory mode - use special function to get OS-pinned memory
#if CUDART_VERSION >= 2020
	cudaHostAlloc((void **)&h_idata, memSize, (wc) ? cudaHostAllocWriteCombined : 0);
	cudaHostAlloc((void **)&h_odata, memSize, (wc) ? cudaHostAllocWriteCombined : 0);
#else
	cudaMallocHost((void **)&h_idata, memSize);
	cudaMallocHost((void **)&h_odata, memSize);
#endif
}
else
{
	//pageable memory mode - use malloc
	h_idata = (unsigned char *)malloc(memSize);
	h_odata = (unsigned char *)malloc(memSize);

	if (h_idata == 0 || h_odata == 0)
	{
	    fprintf(stderr, "Not enough memory avaialable on host to run test!\n");
	    exit(EXIT_FAILURE);
	}
}


//initialize the memory
for (unsigned int i = 0; i < memSize/sizeof(unsigned char); i++)
{
	h_idata[i] = (unsigned char)(i & 0xff);
}

// allocate device memory
unsigned char *d_idata;
cudaMalloc((void **) &d_idata, memSize);


//initialize the device memory
cudaMemcpy(d_idata, h_idata, memSize,cudaMemcpyHostToDevice);

//Allocating unified memory for both Host and Device
cudaMallocManaged(&unified_mem,memSize);



//Copying data from GPU to unified mem
cudaEvent_t GPU_2_Unif_start, GPU_2_Unif_stop;
float GPU_2_Unif_time;

cudaEventCreate(&GPU_2_Unif_start);
cudaEventCreate(&GPU_2_Unif_stop);

cudaEventRecord( GPU_2_Unif_start, 0 );

cudaDeviceSynchronize();



if (PINNED == memMode)
{
	for (unsigned int i = 0; i < MEMCOPY_ITERATIONS; i++)
	{
	    checkCudaErrors(cudaMemcpyAsync(h_odata, d_idata, memSize,
		                            cudaMemcpyDeviceToHost, 0));
	}
}
else
{
	for (unsigned int i = 0; i < MEMCOPY_ITERATIONS; i++)
	{
	    checkCudaErrors(cudaMemcpy(h_odata, d_idata, memSize,
		                       cudaMemcpyDeviceToHost));
	}
}



//get the the total elapsed time in ms
cudaEventRecord( GPU_2_Unif_stop, 0 );
cudaEventSynchronize( GPU_2_Unif_stop );

cudaEventElapsedTime( &GPU_2_Unif_time, GPU_2_Unif_start, GPU_2_Unif_stop );
cudaEventDestroy( GPU_2_Unif_start );
cudaEventDestroy( GPU_2_Unif_stop );


//calculate bandwidth in MB/s
GPU_2_Unif_bandwidthInMBs = ( memSize * (float)MEMCOPY_ITERATIONS) /
             ((float)GPU_2_Unif_time );
printf("Elapsed time is: %f\n",time);
printf("Elapsed time is: %f\n",bandwidthInMBs);


cudaFree(unified_mem);

return 0;
}






