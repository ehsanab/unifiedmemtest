#include <stdlib.h>
#include <stdio.h>
#include <cuda.h>

#define MEMCOPY_ITERATIONS  10000000
#define DEFAULT_SIZE        ( 32 * ( 1 << 20 ) )    //32 M
#define DEFAULT_INCREMENT   (1 << 22)               //4 M
#define CACHE_CLEAR_SIZE    (1 << 24)               //16 M

//shmoo mode defines
#define SHMOO_MEMSIZE_MAX     (1 << 26)         //64 M
#define SHMOO_MEMSIZE_START   (1 << 10)         //1 KB
#define SHMOO_INCREMENT_1KB   (1 << 10)         //1 KB
#define SHMOO_INCREMENT_2KB   (1 << 11)         //2 KB
#define SHMOO_INCREMENT_10KB  (10 * (1 << 10))  //10KB
#define SHMOO_INCREMENT_100KB (100 * (1 << 10)) //100 KB
#define SHMOO_INCREMENT_1MB   (1 << 20)         //1 MB
#define SHMOO_INCREMENT_2MB   (1 << 21)         //2 MB
#define SHMOO_INCREMENT_4MB   (1 << 22)         //4 MB
#define SHMOO_LIMIT_20KB      (20 * (1 << 10))  //20 KB
#define SHMOO_LIMIT_50KB      (50 * (1 << 10))  //50 KB
#define SHMOO_LIMIT_100KB     (100 * (1 << 10)) //100 KB
#define SHMOO_LIMIT_1MB       (1 << 20)         //1 MB
#define SHMOO_LIMIT_16MB      (1 << 24)         //16 MB
#define SHMOO_LIMIT_32MB      (1 << 25)         //32 MB

//enums, project // Imight use these // I got them from bandwidthtest.cu of samples.
enum testMode   { QUICK_MODE, RANGE_MODE, SHMOO_MODE };
enum memcpyKind { DEVICE_TO_HOST, HOST_TO_DEVICE, DEVICE_TO_DEVICE };
enum printMode  { USER_READABLE, CSV };
enum memoryMode { PINNED, PAGEABLE };

using namespace std;
//Source: http://acceleware.com/blog/nvidia-cuda-60-unified-memory-performance
__global__ void  loop (int * a,int*b)//,float * time_out
{

//Note that you can not call CudaMalocMannaged hewr because it is a __host__ function. 

	for ( int i = 0; i < MEMCOPY_ITERATIONS; i++)
	{
		*a=*b;
	}
}


int main ()
{
//int start = DEFAULT_SIZE;
//int end = DEFAULT_SIZE;
int increment = DEFAULT_INCREMENT;

float bandwidthInMBs = 0.0f;
unsigned int memSize = 1;
int a = 5;
int b = 10;

int *p_a, *p_b;


cudaMallocManaged(&p_a,sizeof(int)*memSize);
cudaMallocManaged(&p_b,sizeof(int)*memSize);



cudaEvent_t start, stop;
float time;

cudaEventCreate(&start);
cudaEventCreate(&stop);

cudaEventRecord( start, 0 );

cudaDeviceSynchronize();

//Do some access memory on Host
for ( int i = 0 ; i < MEMCOPY_ITERATIONS; i++)
{

	*p_a=*p_b;

}


//get the the total elapsed time in ms
cudaEventRecord( stop, 0 );
cudaEventSynchronize( stop );

cudaEventElapsedTime( &time, start, stop );
cudaEventDestroy( start );
cudaEventDestroy( stop );


//calculate bandwidth in MB/s
bandwidthInMBs = ( memSize *sizeof(int)* (float)MEMCOPY_ITERATIONS) / ((float)time );
printf("Elapsed time is: %f\n",time);
printf("Bandwidth in MB/s is: %f\n",bandwidthInMBs/1000);



cudaEvent_t start_1, stop_1;
float time_1;

cudaEventCreate(&start_1);
cudaEventCreate(&stop_1);

cudaEventRecord( start_1, 0 );

cudaDeviceSynchronize();

//Do some access memory on Host
for ( int i = 0 ; i < MEMCOPY_ITERATIONS; i++)
{

	*p_a=i;

}


//get the the total elapsed time in ms
cudaEventRecord( stop_1, 0 );
cudaEventSynchronize( stop_1 );

cudaEventElapsedTime( &time_1, start_1, stop_1 );
cudaEventDestroy( start_1 );
cudaEventDestroy( stop_1 );


//calculate bandwidth in MB/s
bandwidthInMBs = ( memSize *sizeof(int)* (float)MEMCOPY_ITERATIONS) / ((float)time_1 );
printf("Elapsed time is: %f\n",time_1);
printf("Bandwidth in MB/s is: %f\n",bandwidthInMBs/1000);



cudaEvent_t start_2, stop_2;
float time_2;
cudaEventCreate(&start_2);
cudaEventCreate(&stop_2);

cudaEventRecord( start_2, 0 );

loop<<<1,1>>>(p_a,p_b);

cudaDeviceSynchronize();

//get the the total elapsed time in ms
cudaEventRecord( stop_2, 0 );
cudaEventSynchronize( stop_2 );

cudaEventElapsedTime( &time_2, start_2, stop_2 );
cudaEventDestroy( start_2 );
cudaEventDestroy( stop_2 );

//calculate bandwidth in MB/s
bandwidthInMBs = ( memSize *sizeof(int)* (float)MEMCOPY_ITERATIONS) / ((float)time_2 );
printf("Elapsed time is: %f\n",time_2); 
printf("Bandwidth in MB/s is: %f\n",bandwidthInMBs/1000);

cudaFree(p_a);
cudaFree(p_b);
return 0;
}






